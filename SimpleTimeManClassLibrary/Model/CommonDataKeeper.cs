﻿using SimpleTimeManClassLibrary.Interfaces;
using System.Collections.Generic;
using System;

namespace SimpleTimeManClassLibrary.Model
{
    public class CommonDataKeeper
    {
        const int WorkDurationBase = 8;
        const int AllDurationBase = 9;
        const string WorkDurationKey = "WorkDurationKey";
        const string AllDurationKey = "AllDurationKey";
        const string AutoEndDayValueKey = "AutoEndDayValueKey";
        iDataWorker DataWorker;
        internal CommonDataKeeper(iDataWorker dataworker)
        {
            DataWorker = dataworker;
        }
        public TimeSpan MaxPausesTime => AllDuration - WorkDuration;
        public TimeSpan WorkDuration
        {
            get
            {
                if (TimeSpan.TryParse(DataWorker.LoadData(WorkDurationKey), out TimeSpan value))
                    return value;
                return TimeSpan.FromHours(WorkDurationBase);
            }
            internal set => DataWorker.SaveData(WorkDurationKey, value.ToString(@"hh\:mm\:ss"));
        }
        public TimeSpan AllDuration
        {
            get
            {
                if (TimeSpan.TryParse(DataWorker.LoadData(AllDurationKey), out TimeSpan value))
                    return value;
                return TimeSpan.FromHours(AllDurationBase);
            }
            internal set => DataWorker.SaveData(AllDurationKey, value.ToString(@"hh\:mm\:ss"));
        }
        public bool AutoEndDayValue
        {
            get
            {
                if (bool.TryParse(DataWorker.LoadData(AutoEndDayValueKey), out bool value))
                    return value;
                return false;
            }
            internal set => DataWorker.SaveData(AutoEndDayValueKey, value.ToString());
        }
        bool rh;
        public bool ReloadHolydays
        {
            get => rh;
            set
            {
                rh = value;
                ReloadedHolydays.Clear();
            }
        }
        internal bool ReloadHolydaysFromYear(int Year)
        {
            if (ReloadHolydays || ReloadedHolydays.Contains(Year))
                return false;
            ReloadedHolydays.Add(Year);
            return true;
        }
        List<int> ReloadedHolydays = new List<int>();
    }
}