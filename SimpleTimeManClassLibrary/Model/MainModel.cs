﻿using SimpleTimeManClassLibrary.Interfaces;
using System;

namespace SimpleTimeManClassLibrary.Model
{
    class MainModel
    {
        static MainModel inst;
        internal static MainModel Instance => inst ?? throw new Exception("Firstly you must Initialize model");
        internal static void Initialize(iDataWorker dataworker, iLogger logger) => inst = new MainModel(dataworker, logger);
        MainModel(iDataWorker dataworker, iLogger logger)
        {
            DataWorker = dataworker;
            Logger = logger;
            CommonDataKeeper = new CommonDataKeeper(dataworker);
        }
        internal readonly iDataWorker DataWorker;
        internal CommonDataKeeper CommonDataKeeper { get; private set; }
        internal iLogger Logger { get; private set; }
    }
}