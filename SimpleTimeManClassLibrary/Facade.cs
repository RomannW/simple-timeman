﻿using SimpleTimeManClassLibrary.Interfaces;
using SimpleTimeManClassLibrary.Classes;
using SimpleTimeManClassLibrary.Model;
using System.Linq;
using System;

namespace SimpleTimeManClassLibrary
{
    public class Facade
    {
        public delegate void VoidChangeHandler();
        public event VoidChangeHandler OnTimesChanged;
        public delegate void StatusesChangeHandler(States state);
        public event StatusesChangeHandler OnStatusChanged;
        public Facade(iDataWorker dataworker, iLogger logger)
        {
            MainModel.Initialize(dataworker, logger);
        }
        public DayInfo GetDayInfo(DateTime date) => new DayInfo(date);
        public string CurretnInfo
        {
            get
            {
                string starttime = "-:--";
                string endtime = "--:--";
                string leavetime = "--:--";
                string progress = "0";
                DayInfo CurrentInfo = GetDayInfo(DateTime.Now);
                if (CurrentInfo.TimeStates.Where(s => s.Status == States.Work).Count() > 0)
                {
                    starttime = CurrentInfo.StartTime.ToString("H:mm");
                    DateTime EndTime;
                    EndTime = CurrentInfo.StartTime + (CurrentInfo.PausesSumm > MainModel.Instance.CommonDataKeeper.MaxPausesTime ?
                        MainModel.Instance.CommonDataKeeper.WorkDuration + CurrentInfo.PausesSumm :
                        MainModel.Instance.CommonDataKeeper.AllDuration);
                    endtime = EndTime.ToString("H:mm");
                    TimeSpan LeaveTime = EndTime - DateTime.Now;
                    IsCurretnDayCompleted = LeaveTime.TotalMinutes <= 0;
                    if (IsCurretnDayCompleted)
                    {
                        IsCurretnDayCompleted = true;
                        leavetime = "[no leave time]";
                        progress = "100";
                    }
                    else
                    {
                        IsCurretnDayCompleted = false;
                        leavetime = LeaveTime.ToString(@"h\:mm");
                        progress = ((1 - LeaveTime.TotalMinutes / MainModel.Instance.CommonDataKeeper.AllDuration.TotalMinutes) * 100).ToString("0.00");

                    }
                }
                return $"Start: {starttime}(End~{endtime})\nLeave: {leavetime}\nProgress: {progress}% ";
            }
        }
        public void EditStateStartTime(TimeState state, DateTime time) => TimeState.EditStateStartTime(state, time);
        public void RemoveTimeState(TimeState state) => TimeState.RemoveTimeState(state);
        public TimeState GetTimeStateFromStartDate(DateTime value) => new DayInfo(value).TimeStates.FirstOrDefault(s => s.StartTime == value);
        public void SetWorkDuration(TimeSpan value) => MainModel.Instance.CommonDataKeeper.WorkDuration = value;
        public void SetAllDuration(TimeSpan value) => MainModel.Instance.CommonDataKeeper.AllDuration = value;
        public bool IsCurretnDayCompleted { get; private set; }
        public TimeState CurretnTimeState => new DayInfo(DateTime.Now).TimeStates.LastOrDefault();
        public TimeSpan WorkDuration => MainModel.Instance.CommonDataKeeper.WorkDuration;
        public TimeSpan AllDuration => MainModel.Instance.CommonDataKeeper.AllDuration;
        public bool AutoEndDayValue => MainModel.Instance.CommonDataKeeper.AutoEndDayValue;
        public bool SetAutoEndDayValue(bool value) => MainModel.Instance.CommonDataKeeper.AutoEndDayValue = value;
        public bool ReloadHolydays => MainModel.Instance.CommonDataKeeper.ReloadHolydays;
        public bool SetReloadHolydays(bool value) => MainModel.Instance.CommonDataKeeper.ReloadHolydays = value;
        public void AddNewCustomEvent(States state, DateTime value) => TimeEvent.AddNewCustomEvent(state, value);
        public void AddNewEvent(States newstate)
        {
            TimeEvent.AddNewEvent(newstate);
            OnStatusChanged?.Invoke(newstate);
            OnTimesChanged?.Invoke();
        }
        public CalendarYearInfo LoadHolydays(DateTime date) => LoadDataHelper.LoadHolydays(date);
        public string[] GetAllLog() => MainModel.Instance.Logger.LoadAllLog();
    }
}