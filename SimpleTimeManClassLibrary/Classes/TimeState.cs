﻿using System.Collections.Generic;
using System;

namespace SimpleTimeManClassLibrary.Classes
{
    public class TimeState
    {
        public DateTime StartTime => StartEvent.Time;
        public States Status => StartEvent.Status;
        public DateTime EndTime
        {
            get
            {
                if (EndEvent != null)
                    return EndEvent.Time;
                if(StartTime.Date == DateTime.Now.Date)
                    return DateTime.Now;
                return StartTime;
            }
        }
        public TimeSpan Duration => EndTime - StartTime;
        public bool IsEndlessState => EndEvent == null && StartTime.Date != DateTime.Now.Date;
        TimeEvent StartEvent { get; set; }
        TimeEvent EndEvent { get; set; }
        TimeState(TimeEvent startevent, TimeEvent endevent = null)
        {
            StartEvent = startevent;
            EndEvent = endevent;
        }
        internal static TimeState[] CreateStates(TimeEvent[] events)
        {
            List<TimeState> TimeStates = new List<TimeState>();
            for (int i = 0; i < events.Length; i++)
            {
                TimeEvent endevent = i < events.Length - 1 ? events[i + 1] : null;
                TimeStates.Add(new TimeState(events[i], endevent));
            }
            return TimeStates.ToArray();
        }
        internal static void RemoveTimeState(TimeState state)
        {
            TimeEvent.RemoveTimeEvent(state.StartEvent);
            if(state.EndEvent != null)
                TimeEvent.RemoveTimeEvent(state.EndEvent);
        }
        internal static void EditStateStartTime(TimeState state, DateTime time)
        {
            TimeEvent.EditEventTime(state.StartEvent, time);
        }
    }
}