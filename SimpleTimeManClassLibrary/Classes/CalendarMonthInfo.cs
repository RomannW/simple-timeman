﻿using System.Collections.Generic;
using System;

namespace SimpleTimeManClassLibrary.Classes
{
    public class CalendarMonthInfo : BaseCalendarItem
    {
        internal CalendarMonthInfo(DateTime date, int month) : base(month)
        {
            List<CalendarDayInfo> temp = new List<CalendarDayInfo>();
            int DaysInThisMonth = new DateTime(date.Year, month, 1).AddMonths(1).AddDays(-1).Day;
            for (int i = 0; i < DaysInThisMonth;)
                if(IsHolyDay(new DateTime(date.Year, month, ++i).DayOfWeek))
                    temp.Add(new CalendarDayInfo(i.ToString()));
            Days = temp.ToArray();
        }
        bool IsHolyDay(DayOfWeek day) => day == DayOfWeek.Saturday || day == DayOfWeek.Sunday;
        internal CalendarMonthInfo(DateMonthInfo info) : base(int.Parse(info.Month))
        {
            List<CalendarDayInfo> temp = new List<CalendarDayInfo>();
            foreach (string day in info.Days.Split(','))
                temp.Add(new CalendarDayInfo(day));
            Days = temp.ToArray();
        }
        public int Month => Name;
        public CalendarDayInfo[] Days { get; private set; }
    }
}