﻿namespace SimpleTimeManClassLibrary.Classes
{
    public class BaseCalendarItem
    {
        protected BaseCalendarItem(int name)
        {
            Name = name;
        }
        protected int Name { get; private set; }
    }
}