﻿using System.Linq;
using System;

namespace SimpleTimeManClassLibrary.Classes
{
    public class DayInfo
    {
        public TimeState[] TimeStates { get; private set; }
        public DateTime Date { get; private set; }
        public DateTime StartTime => TimeStates?.FirstOrDefault()?.StartTime ?? Date;
        public DateTime EndTime => TimeStates?.LastOrDefault()?.EndTime ?? Date;
        public TimeSpan Duration => EndTime - StartTime;
        public TimeSpan PausesSumm => TimeSpan.FromSeconds(TimeStates?.Where(s => s.Status == States.WorkPaused).Select(s => s.Duration.TotalSeconds).Sum() ?? 0);
        public States LastState => TimeStates.Length > 0 ? TimeStates.LastOrDefault().Status : States.WorkEnded;
        internal DayInfo(DateTime date)
        {
            Date = date.Date;
            TimeStates = TimeState.CreateStates(TimeEvent.GetTimeEvents(date));
        }
    }
}