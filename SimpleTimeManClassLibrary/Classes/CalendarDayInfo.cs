﻿namespace SimpleTimeManClassLibrary.Classes
{
    public class CalendarDayInfo : BaseCalendarItem
    {
        public enum DayTypes { Short, DayOff, Work };
        internal CalendarDayInfo(int day) : base(day)
        {
            DayType = DayTypes.DayOff;
        }
        internal CalendarDayInfo(string day) : base(int.Parse(day.Replace("*", "")))
        {
            if (day.Contains("*"))
                DayType = DayTypes.Short;
            else
                DayType = DayTypes.DayOff;
        }
        public int Day => Name;
        public DayTypes DayType { get; private set; }
    }
}