﻿using SimpleTimeManClassLibrary.Model;
using System.Runtime.Serialization;
using System.Net;
using System;


namespace SimpleTimeManClassLibrary.Classes
{
    class LoadDataHelper
    {
        static object Locker = new object();
        const string BaseUri = @"http://xmlcalendar.ru/";
        static string CreateUri(DateTime date) => $"{BaseUri}data/ru/{date.Year}/calendar.json";
        internal static CalendarYearInfo LoadHolydays(DateTime date)
        {
            lock (Locker)
            {
                string Data = LoadData(date);
                if (Data != "")
                {
                    SaveCash(CreateUri(date), Data);
                    return new CalendarYearInfo(JSONHelper.ParseDataYearInfo(Data));
                }
                return new CalendarYearInfo(date);
            }
        }
        static string LoadData(DateTime date)
        {
            string Uri = CreateUri(date);
            if (MainModel.Instance.CommonDataKeeper.ReloadHolydaysFromYear(date.Year) && LoadFromCash(Uri, out string CashedData))
                return CashedData;
            else if (TryLoadJsonString(Uri, out string LoadedData))
                return LoadedData;
            return "";
        }
        static bool TryLoadJsonString(string Uri, out string result)
        {
            try
            {
                using (var wc = new WebClient())
                    result = wc.DownloadString(Uri);
                MainModel.Instance.Logger.WriteLog("TryLoadJsonString: " + result);
            }
            catch (Exception Ex)
            {
                MainModel.Instance.Logger.WriteLog("TryLoadJsonString Ex: " + Ex.Message);
                result = "";
            }
            return result != "";
        }
        static bool LoadFromCash(string uri, out string result)
        {
            result = MainModel.Instance.DataWorker.LoadData(uri, "");
            MainModel.Instance.Logger.WriteLog("LoadFromCash: " + result);
            return result != "";
        }
        static void SaveCash(string uri, string data)
        {
            MainModel.Instance.Logger.WriteLog("SaveCash: " + data);
            MainModel.Instance.DataWorker.SaveData(uri, data);
        }
    }
    [DataContract]
    internal class DataYearInfo
    {
        [DataMember(Name = "year")]
        internal string Year { get; set; }
        [DataMember(Name = "months")]
        internal DateMonthInfo[] Months { get; set; }
    }
    [DataContract]
    internal class DateMonthInfo
    {
        [DataMember(Name = "month")]
        internal string Month { get; set; }
        [DataMember(Name = "days")]
        internal string Days { get; set; }
    }
}