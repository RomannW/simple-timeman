﻿using System.Collections.Generic;
using System;

namespace SimpleTimeManClassLibrary.Classes
{
    public class CalendarYearInfo : BaseCalendarItem
    {
        internal CalendarYearInfo(DateTime date) : base(date.Year)
        {
            List<CalendarMonthInfo> temp = new List<CalendarMonthInfo>();
            for (int i = 0; i < 12; )
                temp.Add(new CalendarMonthInfo(date, ++i));
            Months = temp.ToArray();
        }
        internal CalendarYearInfo(DataYearInfo info) : base(int.Parse(info.Year))
        {
            List<CalendarMonthInfo> temp = new List<CalendarMonthInfo>();
            foreach (DateMonthInfo monthinfo in info.Months)
                temp.Add(new CalendarMonthInfo(monthinfo));
            Months = temp.ToArray();
        }
        public int Year => Name;
        public CalendarMonthInfo[] Months { get; private set; }
    }
}