﻿using System.Runtime.Serialization.Json;
using SimpleTimeManClassLibrary.Model;
using System.IO;
using System;

namespace SimpleTimeManClassLibrary.Classes
{
    class JSONHelper
    {
        internal static DataYearInfo ParseDataYearInfo(string data)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(data)))
                    return new DataContractJsonSerializer(typeof(DataYearInfo)).ReadObject(ms) as DataYearInfo;
            }
            catch (Exception Ex)
            {
                MainModel.Instance.Logger.WriteLog("ParseDataYearInfo Ex: " + Ex.Message);
                return null;
            }
        }
    }
}