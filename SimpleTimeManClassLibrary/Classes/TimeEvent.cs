﻿using SimpleTimeManClassLibrary.Model;
using System.Collections.Generic;
using System.Linq;
using System;

namespace SimpleTimeManClassLibrary.Classes
{
    class TimeEvent
    {
        const string Separator = ";";
        static string[] SeparatorArray => new string[] { Separator };
        internal const string TimeEventTag = "TimeEventTag";
        internal DateTime Time { get; private set; }
        internal States Status { get; private set; }
        internal int ID { get; private set; }
        TimeEvent(States state)
        {
            Time = DateTime.Now;
            Status = state;
        }
        TimeEvent(States state, DateTime date)
        {
            Time = date;
            Status = state;
        }
        TimeEvent(string rawdata)
        {
            if (rawdata.Length != 24)
                throw new Exception(rawdata + " Length != 24");
            ID = int.Parse(rawdata.Substring(0, 4));
            Time = ParseString(rawdata.Substring(5, 17));
            if (int.TryParse(rawdata.Substring(23, 1), out int type))
                Status = (States)type;
            else
                throw new Exception(rawdata.Substring(9, 1) + " cant parse as int");
        }
        DateTime ParseString(string datestring)
        {
            if (int.TryParse(datestring.Substring(0, 2), out int day))
                if (int.TryParse(datestring.Substring(3, 2), out int month))
                    if (int.TryParse(datestring.Substring(6, 2), out int year))
                        if (int.TryParse(datestring.Substring(9, 2), out int hour))
                            if (int.TryParse(datestring.Substring(12, 2), out int minute))
                                if (int.TryParse(datestring.Substring(15, 2), out int second))
                                    return new DateTime(2000 + year, month, day, hour, minute, second);
            throw new Exception(datestring + " cant parse as DateTime");
        }
        internal static void EditEventTime(TimeEvent tevent, DateTime time)
        {
            List<TimeEvent> Events = GetTimeEvents(time).ToList();
            TimeEvent CurrentEvent = Events.FirstOrDefault(e => e.Time == tevent.Time);
            if (CurrentEvent != null)
            {
                Events[Events.IndexOf(CurrentEvent)].Time = time;
                SaveTimeEvents(Events.ToArray(), time);
            }
        }
        internal static void RemoveTimeEvent(TimeEvent tevent)
        {
            List<TimeEvent> Events = GetTimeEvents(tevent.Time).ToList();
            Events.Remove(Events.FirstOrDefault(e => e.Time == tevent.Time));
            SaveTimeEvents(Events.ToArray(), tevent.Time);
        }
        internal string GetString() => ID.ToString("0000") + "_" + Time.ToString("dd.MM.yy HH:mm:ss") + "_" + ((int)Status).ToString();
        internal static TimeEvent[] GetTimeEvents(DateTime date)
        {
            List<TimeEvent> TimeEvents = new List<TimeEvent>();
            string RawData = MainModel.Instance.DataWorker.LoadData(CreateTag(date));
            foreach (string rawdataitem in MainModel.Instance.DataWorker.LoadData(CreateTag(date)).Split(SeparatorArray, StringSplitOptions.RemoveEmptyEntries))
                TimeEvents.Add(new TimeEvent(rawdataitem));
            return TimeEvents.ToArray();
        }
        internal static void AddNewEvent(States state)
        {
            List<TimeEvent> TodayEvents = GetTimeEvents(DateTime.Now).ToList();
            TimeEvent NewEvent = new TimeEvent(state);
            NewEvent.ID = GetTimeEvents(DateTime.Now).LastOrDefault()?.ID ?? 0;
            TodayEvents.Add(NewEvent);
            SaveTimeEvents(TodayEvents.ToArray(), DateTime.Now);
        }
        internal static void AddNewCustomEvent(States state, DateTime date)
        {
            List<TimeEvent> TodayEvents = GetTimeEvents(date).ToList();
            TimeEvent NewEvent = new TimeEvent(state, date);
            TodayEvents.Add(NewEvent);
            int id = 0;
            foreach (TimeEvent Event in TodayEvents)
                Event.ID = id++;
            SaveTimeEvents(TodayEvents.ToArray(), date);
        }
        static void SaveTimeEvents(TimeEvent[] events, DateTime date)
        {
            MainModel.Instance.DataWorker.SaveData(CreateTag(date), string.Join(Separator, events.Select(e => e.GetString())));
        }
        static string CreateTag(DateTime date) => "DayInfo_" + date.ToString("dd.MM.yyyy");
    }
}