﻿using System.Runtime.CompilerServices;

namespace SimpleTimeManClassLibrary.Interfaces
{
    public interface iLogger
    {
        void WriteLog(string data,
            [CallerLineNumber] int num = -1,
            [CallerFilePath] string path = "",
            [CallerMemberName] string name = "");
        string[] LoadAllLog();
    }
}