﻿namespace SimpleTimeManClassLibrary.Interfaces
{
    public interface iDataWorker
    {
        string LoadData(string key, string def = "");
        void SaveData(string key, string value);
    }
}