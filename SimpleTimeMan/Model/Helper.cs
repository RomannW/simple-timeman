﻿using Android.Graphics;

namespace SimpleTimeMan.Model
{
    class Helper
    {
        static Color WorkColor => Color.Rgb(0x88, 0xFF, 0x88);
        static Color PauseColor => Color.Rgb(0x44, 0xAA, 0xFF);
        static Color EndColor => Color.Rgb(0xFF, 0x88, 0x88);
        internal static Color GetColorByState(States status)
        {
            switch (status)
            {
                case States.WorkEnded:
                    return EndColor;
                case States.Work:
                    return WorkColor;
                case States.WorkPaused:
                default:
                    return PauseColor;
            }
        }
    }
}