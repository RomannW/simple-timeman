﻿using SimpleTimeManClassLibrary.Classes;
using SimpleTimeManClassLibrary;
using SimpleTimeMan.Classes;
using System.Timers;
using System;

namespace SimpleTimeMan.Model
{
    class MainModel
    {
        internal delegate void VoidChangeHandler();
        internal static event VoidChangeHandler OnTimesChanged;
        internal delegate void StatusesChangeHandler(States state);
        internal static event StatusesChangeHandler OnStatusChanged;
        static Facade Facade;
        static Timer Timer;
        static MainModel()
        {
            DataWorker DataWorker = new DataWorker();
            Facade = new Facade(DataWorker, new Logger(DataWorker));
            Facade.OnStatusChanged += Instance_OnStatusChanged;
            Facade.OnTimesChanged += Instance_OnTimesChanged;
            Timer = new Timer(5000);
            Timer.Elapsed += (s, e) => OnTimesChanged?.Invoke();
            Timer.Start();
        }
        static void Instance_OnTimesChanged() => OnTimesChanged?.Invoke();
        static void Instance_OnStatusChanged(States state) => OnStatusChanged?.Invoke(state);
        internal static DayInfo GetDayInfo(DateTime date) => Facade.GetDayInfo(date);
        internal static void StartWork() => Facade.AddNewEvent(States.Work);
        internal static void PauseWork() => Facade.AddNewEvent(States.WorkPaused);
        internal static void EndWork() => Facade.AddNewEvent(States.WorkEnded);
        internal static string CurretnInfo => Facade.CurretnInfo;
        internal static bool IsDayCompleted => Facade.IsCurretnDayCompleted;
        internal static TimeState GetTimeStateFromStartDate(DateTime value) => Facade.GetTimeStateFromStartDate(value);
        internal static TimeSpan WorkDuration => Facade.WorkDuration;
        internal static TimeSpan AllDuration => Facade.AllDuration;
        internal static bool AutoEndDayValue => Facade.AutoEndDayValue;
        internal static void SetAutoEndDayValue(bool value) => Facade.SetAutoEndDayValue(value);
        internal static bool ReloadHolydays => Facade.ReloadHolydays;
        internal static void SetReloadHolydays(bool value) => Facade.SetReloadHolydays(value);
        internal static States CurretnState => GetDayInfo(DateTime.Now).LastState;
        internal static void SetAllDuration(TimeSpan value) => Facade.SetAllDuration(value);
        internal static void SetWorkDuration(TimeSpan value) => Facade.SetWorkDuration(value);
        internal static void EditStateStartTime(TimeState state, DateTime time) => Facade.EditStateStartTime(state, time);
        internal static void AddNewCustomEvent(States state, DateTime value) => Facade.AddNewCustomEvent(state, value);
        internal static void RemoveTimeState(TimeState state) => Facade.RemoveTimeState(state);
        internal static CalendarYearInfo LoadHolydays(DateTime date) => Facade.LoadHolydays(date);
        internal static string[] GetAllLog() => Facade.GetAllLog();
    }
}