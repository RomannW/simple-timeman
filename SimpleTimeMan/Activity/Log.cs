﻿using System.Threading.Tasks;
using SimpleTimeMan.Model;
using System.Threading;
using Android.Graphics;
using Android.Widget;
using Android.App;
using Android.OS;
using System;


namespace SimpleTimeMan.Activity
{
    [Activity(
       Label = "@string/app_name")]
    public class Log : CustomActivity
    {
        ImageButton GoBack;
        TextView LogPlace;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_log);
            GoBack = FindViewById<ImageButton>(Resource.Id.GoBack);
            LogPlace = FindViewById<TextView>(Resource.Id.LogPlace);
        }
        protected override void OnStart()
        {
            base.OnStart();
            GoBack.Click += GoBack_Click;
        }
        internal override void Refrash()
        {
            LogPlace.Text = string.Join("\n\n", MainModel.GetAllLog());
        }
        protected override void OnStop()
        {
            GoBack.Click -= GoBack_Click;
            base.OnStop();
        }
        void GoBack_Click(object s, EventArgs e)
        {
            OnBackPressed();
        }
    }
}