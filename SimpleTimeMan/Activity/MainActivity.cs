﻿using SimpleTimeMan.Classes;
using SimpleTimeMan.Model;
using Android.Widget;
using Android.App;
using Android.OS;
using System;

namespace SimpleTimeMan.Activity
{
    [Activity(
        Label = "@string/app_name",
        MainLauncher = true
        )]
    public class MainActivity : CustomActivity
    {
        TextView StatusLabel, StateLabel;
        Button StartPauseButton;
        ImageButton GoEventList;
        ImageButton Settings;
        CustomCircleTimeLine CircleDiagram; 
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            StatusLabel = FindViewById<TextView>(Resource.Id.StatusLabel);
            StateLabel = FindViewById<TextView>(Resource.Id.StateLabel);
            StartPauseButton = FindViewById<Button>(Resource.Id.StartPauseButton);
            GoEventList = FindViewById<ImageButton>(Resource.Id.GoEventList);
            Settings = FindViewById<ImageButton>(Resource.Id.Settings);
            CircleDiagram = FindViewById<CustomCircleTimeLine>(Resource.Id.CircleDiagram);

            //CreateWork(new DateTime(2021, 2, 3));
            //CreateWork(new DateTime(2021, 2, 4));

            //CreateWork(new DateTime(2021, 2, 9));
            //CreateWork(new DateTime(2021, 2, 10));
            //CreateWork(new DateTime(2021, 2, 11));
            //CreateWork(new DateTime(2021, 2, 12));

            //CreateWork(new DateTime(2021, 2, 15));
            //CreateWork(new DateTime(2021, 2, 16));
            //CreateWork(new DateTime(2021, 2, 17));
            //CreateWork(new DateTime(2021, 2, 18));
            //CreateWork(new DateTime(2021, 2, 19));

            //CreateWork(new DateTime(2021, 3, 1));
            //CreateWork(new DateTime(2021, 3, 2));
            //CreateWork(new DateTime(2021, 3, 3));
        }
        //void CreateWork(DateTime Date)
        //{
        //    MainModel_Old.Instance.LoadData(Date);
        //    MainModel_Old.Instance.SetCustomTimeStamp(Statuses.Work, Date.Date.AddHours(10));
        //    MainModel_Old.Instance.SetCustomTimeStamp(Statuses.WorkEnded, Date.Date.AddHours(19));
        //}
        protected override void OnStart()
        {
            base.OnStart();
            StartPauseButton.Click += StartPauseButton_Click;
            GoEventList.Click += GoEventList_Click;
            Settings.Click += Settings_Click;
            MainModel.OnStatusChanged += MainModel_OnStatusChanged;
            MainModel.OnTimesChanged += MainModel_OnTimesChanged;
        }
        internal override void Refrash()
        {
            StateLabel.Text = MainModel.CurretnInfo;
            MainModel_OnStatusChanged(MainModel.CurretnState);
            CircleDiagram.TimeStates = MainModel.GetDayInfo(DateTime.Now).TimeStates;
            if (MainModel.AutoEndDayValue && MainModel.CurretnState == States.Work && MainModel.GetDayInfo(DateTime.Now).Duration >= MainModel.AllDuration)
                MainModel.EndWork();
        }
        protected override void OnStop()
        {
            base.OnStop();
            StartPauseButton.Click -= StartPauseButton_Click;
            GoEventList.Click -= GoEventList_Click;
            Settings.Click -= Settings_Click;
            MainModel.OnStatusChanged -= MainModel_OnStatusChanged;
            MainModel.OnTimesChanged -= MainModel_OnTimesChanged;
        }
        void StartPauseButton_Click(object s, EventArgs e)
        {
            if (MainModel.CurretnState != States.Work)
                MainModel.StartWork();
            else if (MainModel.IsDayCompleted)
                MainModel.EndWork();
            else
                MainModel.PauseWork();
        }
        void GoEventList_Click(object sender, EventArgs e) => StartActivity(typeof(EventList));
        void Settings_Click(object sender, EventArgs e) => StartActivity(typeof(Settings));
        void MainModel_OnStatusChanged(States state)
        {
            CircleDiagram.TimeStates = MainModel.GetDayInfo(DateTime.Now).TimeStates;
            StatusLabel.SetTextColor(Helper.GetColorByState(state));
            StatusLabel.Text = state.ToString();
            switch (state)
            {
                case States.WorkEnded:
                    StartPauseButton.Text = "Start";
                    break;
                case States.WorkPaused:
                    StartPauseButton.Text = "Start";
                    break;
                case States.Work:
                default:
                    StartPauseButton.Text = MainModel.IsDayCompleted ? "End" : "Pause";
                    break;
            }
        }
        void MainModel_OnTimesChanged() => RunOnUiThread(() => StateLabel.Text = MainModel.CurretnInfo);
    }
}