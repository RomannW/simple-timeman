﻿using Android.Support.V7.App;
using Android.OS;

namespace SimpleTimeMan.Activity
{
    public abstract class CustomActivity : AppCompatActivity
    {
        public static CustomActivity Context { get; private set; }
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Context = this;
        }
        protected override void OnStart()
        {
            base.OnStart();
            Refrash();
        }
        internal abstract void Refrash();
    }
}