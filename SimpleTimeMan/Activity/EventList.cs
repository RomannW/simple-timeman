﻿using SimpleTimeManClassLibrary.Classes;
using SimpleTimeMan.Classes.Exctention;
using SimpleTimeMan.Model;
using Android.Widget;
using System.Linq;
using Android.App;
using Android.OS;
using System;

namespace SimpleTimeMan.Activity
{
    [Activity(
    Label = "@string/app_name")]
    public class EventList : CustomActivity
    {
        public const string DateTag = "DateTag";
        ImageButton GoBack, OpenCalendar;
        Button AddEvent, AddWork;
        LinearLayout EventsPlace;
        DateTime Date = DateTime.Now.Date;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_eventlist);
            GoBack = FindViewById<ImageButton>(Resource.Id.GoBack);
            OpenCalendar = FindViewById<ImageButton>(Resource.Id.OpenCalendar);
            EventsPlace = FindViewById<LinearLayout>(Resource.Id.EventsPlace);
            AddEvent = FindViewById<Button>(Resource.Id.AddEvent);
            AddWork = FindViewById<Button>(Resource.Id.AddWork);
                
            if(Intent.Extras != null && DateTime.TryParse(Intent.Extras.GetString(DateTag, ""), out DateTime date))
                Date = date;
            if (Date.Date == DateTime.Now.Date)
                AddEvent.Visibility = AddWork.Visibility = Android.Views.ViewStates.Gone;
        }
        protected override void OnStart()
        {
            base.OnStart();
            GoBack.Click += GoBack_Click;
            AddEvent.Click += AddEvent_Click;
            AddWork.Click += AddWork_Click;
            OpenCalendar.Click += OpenCalendar_Click;
            Refrash();
        }
        internal override void Refrash()
        {
            EventsPlace.RemoveAllViews();
            foreach (TimeState State in MainModel.GetDayInfo(Date).TimeStates)
                EventsPlace.AddView(State.GetView(this, EventsPlace));
        }
        void AddEvent_Click(object sender, EventArgs e)
        {
            DayInfo DayInfo = MainModel.GetDayInfo(Date);
            if (DayInfo.TimeStates.Length == 0)
                MainModel.AddNewCustomEvent(States.Work, Date.Date.AddHours(10));
            else if (DayInfo.LastState == States.WorkEnded || DayInfo.LastState == States.WorkPaused)
                MainModel.AddNewCustomEvent(States.Work, DayInfo.TimeStates.Last().StartTime.AddHours(1));
            else if (DayInfo.Duration < MainModel.AllDuration)
                MainModel.AddNewCustomEvent(States.WorkPaused, DayInfo.TimeStates.Last().StartTime.AddHours(1));
            else
                MainModel.AddNewCustomEvent(States.WorkEnded, DayInfo.TimeStates.Last().StartTime.AddHours(1));

            EventsPlace.RemoveAllViews();
            foreach (TimeState State in MainModel.GetDayInfo(Date).TimeStates)
                EventsPlace.AddView(State.GetView(this, EventsPlace));
            Refrash();
        }
        void AddWork_Click(object s, EventArgs e)
        {
            DayInfo DayInfo = MainModel.GetDayInfo(Date);
            if (DayInfo.TimeStates.Length == 0)
            {
                MainModel.AddNewCustomEvent(States.Work, Date.Date.AddHours(10));
                MainModel.AddNewCustomEvent(States.WorkEnded, Date.Date.AddHours(10 + MainModel.AllDuration.TotalHours));
            }
            Refrash();
        }
        void OpenCalendar_Click(object sender, EventArgs e) => StartActivity(typeof(Calendar));
        protected override void OnStop()
        {
            EventsPlace.RemoveAllViews();
            GoBack.Click -= GoBack_Click;
            AddEvent.Click -= AddEvent_Click;
            AddWork.Click -= AddWork_Click;
            OpenCalendar.Click -= OpenCalendar_Click;
            base.OnStop();
        }
        void GoBack_Click(object sender, EventArgs e)
        {
            OnBackPressed();
        }
    }
}