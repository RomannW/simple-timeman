﻿using System.Threading.Tasks;
using SimpleTimeMan.Model;
using System.Threading;
using Android.Graphics;
using Android.Widget;
using Android.App;
using Android.OS;
using System;

namespace SimpleTimeMan.Activity
{
    [Activity(
       Label = "@string/app_name")]
    public class Settings : CustomActivity
    {
        ImageButton GoBack, OpenLog;
        Button ApplySetting;
        SeekBar WorkTimeSetting, AllTimeSetting;
        TextView WorkTimeText, AllTimeText;
        CheckBox AutoEndCheckBox, ReloadHolydaysCheckBox;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_settings);
            GoBack = FindViewById<ImageButton>(Resource.Id.GoBack);
            OpenLog = FindViewById<ImageButton>(Resource.Id.OpenLog);
            ApplySetting = FindViewById<Button>(Resource.Id.ApplySetting);
            WorkTimeSetting = FindViewById<SeekBar>(Resource.Id.WorkTimeSetting);
            AllTimeSetting = FindViewById<SeekBar>(Resource.Id.AllTimeSetting);
            WorkTimeText = FindViewById<TextView>(Resource.Id.WorkTimeText);
            AllTimeText = FindViewById<TextView>(Resource.Id.AllTimeText);
            AutoEndCheckBox = FindViewById<CheckBox>(Resource.Id.AutoEndCheckBox);
            ReloadHolydaysCheckBox = FindViewById<CheckBox>(Resource.Id.ReloadHolydaysCheckBox);
        }
        protected override void OnStart()
        {
            base.OnStart();
            GoBack.Click += GoBack_Click;
            OpenLog.Click += OpenLog_Click;
            ApplySetting.Click += ApplySetting_Click;
            WorkTimeSetting.ProgressChanged += WorkTimeSetting_ProgressChanged;
            AllTimeSetting.ProgressChanged += AllTimeSetting_ProgressChanged;
        }
        internal override void Refrash()
        {
            WorkTimeSetting.Progress = (int)(MainModel.WorkDuration.TotalHours * 2);
            AllTimeSetting.Progress = (int)(MainModel.AllDuration.TotalHours * 2);
            AutoEndCheckBox.Checked = MainModel.AutoEndDayValue;
            ReloadHolydaysCheckBox.Checked = MainModel.ReloadHolydays;
        }
        protected override void OnStop()
        {
            GoBack.Click -= GoBack_Click;
            OpenLog.Click -= OpenLog_Click;
            ApplySetting.Click -= ApplySetting_Click;
            WorkTimeSetting.ProgressChanged -= WorkTimeSetting_ProgressChanged;
            AllTimeSetting.ProgressChanged -= AllTimeSetting_ProgressChanged;
            base.OnStop();
        }
        void GoBack_Click(object s, EventArgs e)
        {
            OnBackPressed();
        }
        void OpenLog_Click(object sender, EventArgs e) => StartActivity(typeof(Log));
        void ApplySetting_Click(object s, EventArgs e)
        {
            if (AllTimeSetting.Progress > 0 && WorkTimeSetting.Progress > 0)
            {
                MainModel.SetAllDuration(TimeSpan.FromHours(AllTimeSetting.Progress / 2.0F));
                MainModel.SetWorkDuration(TimeSpan.FromHours(WorkTimeSetting.Progress / 2.0F));
                MainModel.SetAutoEndDayValue(AutoEndCheckBox.Checked);
                MainModel.SetReloadHolydays(ReloadHolydaysCheckBox.Checked);
                OnBackPressed();
            }
            else
            {
                if (AllTimeSetting.Progress < 0)
                {
                    AllTimeText.SetTextColor(Color.Red);
                    Task.Run(() =>
                    {
                        Thread.Sleep(750);
                        AllTimeText.SetTextColor(Color.White);
                    });
                }
                if (WorkTimeSetting.Progress < 0)
                {
                    WorkTimeText.SetTextColor(Color.Red);
                    Task.Run(() =>
                    {
                        Thread.Sleep(750);
                        WorkTimeText.SetTextColor(Color.White);
                    });
                }
            }
        }
        void WorkTimeSetting_ProgressChanged(object s, SeekBar.ProgressChangedEventArgs e)
        {
            string wt = e.Progress % 2 == 0 ? (e.Progress / 2).ToString() : (e.Progress / 2.0F).ToString("0.0");
            WorkTimeText.Text = "Work time(" + wt + "h)";
            if (AllTimeSetting.Progress < WorkTimeSetting.Progress)
                AllTimeSetting.Progress = WorkTimeSetting.Progress;
        }
        void AllTimeSetting_ProgressChanged(object s, SeekBar.ProgressChangedEventArgs e)
        {
            string at = e.Progress % 2 == 0 ? (e.Progress / 2).ToString() : (e.Progress / 2.0F).ToString("0.0");
            AllTimeText.Text = "All time(" + at + "h)";
            if (WorkTimeSetting.Progress > AllTimeSetting.Progress)
                WorkTimeSetting.Progress = AllTimeSetting.Progress;
        }
    }
}