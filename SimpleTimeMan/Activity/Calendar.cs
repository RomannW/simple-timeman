﻿using System.Collections.Generic;
using SimpleTimeMan.Classes;
using Android.Animation;
using Android.Widget;
using Android.App;
using Android.OS;
using System;

namespace SimpleTimeMan.Activity
{
    [Activity(
        Label = "@string/app_name")]
    public class Calendar : CustomActivity
    {
        TextView CurrentYear, NextYear, CurrentMonth, NextMonth, StatisticsView;
        ImageButton GoBack;
        DateTime CurrentDate = DateTime.Now.Date;
        LinearLayout[] Weeks;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_calendar);
            GoBack = FindViewById<ImageButton>(Resource.Id.GoBack);
            CurrentYear = FindViewById<TextView>(Resource.Id.CurrentYear);
            NextYear = FindViewById<TextView>(Resource.Id.NextYear);
            CurrentMonth = FindViewById<TextView>(Resource.Id.CurrentMonth);
            NextMonth = FindViewById<TextView>(Resource.Id.NextMonth);
            StatisticsView = FindViewById<TextView>(Resource.Id.StatisticsView);
            Weeks = new LinearLayout[]
            {
                FindViewById<LinearLayout>(Resource.Id.Week0),
                FindViewById<LinearLayout>(Resource.Id.Week1),
                FindViewById<LinearLayout>(Resource.Id.Week2),
                FindViewById<LinearLayout>(Resource.Id.Week3),
                FindViewById<LinearLayout>(Resource.Id.Week4),
                FindViewById<LinearLayout>(Resource.Id.Week5)
            };
        }
        protected override void OnStart()
        {
            base.OnStart();
            GoBack.Click += GoBack_Click;
            NextYear.Alpha = NextMonth.Alpha = 0;
            CurrentMonth.Touch += CurrentMonth_Touch;
        }
        internal override void Refrash()
        {
            CurrentYear.Text = CurrentDate.ToString("yyyy");
            NextYear.Text = CurrentDate.AddYears(1).ToString("yyyy");
            CurrentMonth.Text = CurrentDate.ToString("MMMM");
            NextMonth.Text = CurrentDate.AddMonths(1).ToString("MMMM");
            CurrentYear.Alpha = CurrentMonth.Alpha = 1.0F;
            NextYear.Alpha = NextMonth.Alpha = 0.0F;

            List<CalendarDay> CalendarDays = CalendarDay.GetMonth(CurrentDate);
            foreach (LinearLayout Week in Weeks)
                Week.RemoveAllViews();
            foreach (CalendarDay CalendarDay in CalendarDays)
                Weeks[CalendarDay.Week].AddView(CalendarDay.GetView(this, Weeks[CalendarDay.Week]));

            StatisticsView.Text = CalendarDay.GetStatisticsMonth(CalendarDays, CurrentDate);
        }
        protected override void OnStop()
        {
            CurrentMonth.Touch -= CurrentMonth_Touch;
            GoBack.Click -= GoBack_Click;
            base.OnStop();
        }
        void GoBack_Click(object s, EventArgs e)
        {
            OnBackPressed();
        }
        float StartX = 0.0F;
        void CurrentMonth_Touch(object s, Android.Views.View.TouchEventArgs e)
        {
            float GetDelta() => e.Event.RawX - StartX;
            if (e.Event.Action == Android.Views.MotionEventActions.Down)
            {
                StartX = e.Event.RawX;
            }
            else if (e.Event.Action == Android.Views.MotionEventActions.Up || e.Event.Action == Android.Views.MotionEventActions.Cancel)
            {
                if (Math.Abs(GetDelta()) > 200)
                //{
                    CurrentDate = CurrentDate.AddMonths(-Math.Sign(GetDelta()));
                //    ValueAnimator CurrentMonthBacker = ValueAnimator.OfFloat(GetDelta(), 0);
                //    CurrentMonthBacker.SetDuration((int)Math.Abs(GetDelta()));
                //    CurrentMonthBacker.Update += (s, e) => ((FrameLayout.LayoutParams)CurrentMonth.LayoutParameters).SetMargins((int)e.Animation.AnimatedValue / 3, 0, -(int)e.Animation.AnimatedValue / 3, 0);
                //    CurrentMonthBacker.Start();
                //}
                //else
                //{
                    ((FrameLayout.LayoutParams)CurrentMonth.LayoutParameters).SetMargins(0, 0, 0, 0);
                //}
                Refrash();
            }
            else
            {
                NextMonth.Text = CurrentDate.AddMonths(Math.Sign(-GetDelta())).ToString("MMMM");
                NextYear.Text = CurrentDate.AddMonths(Math.Sign(-GetDelta())).ToString("yyyy");
                CurrentYear.Alpha = CurrentMonth.Alpha = (200.0F - Math.Abs(GetDelta())) / 200.0F;
                NextYear.Alpha = NextMonth.Alpha = Math.Abs(GetDelta()) / 200.0F;
                ((FrameLayout.LayoutParams)CurrentMonth.LayoutParameters).SetMargins((int)GetDelta() / 3, 0, -(int)GetDelta() / 3, 0);
            }
        }
    }
}