﻿using SimpleTimeManClassLibrary.Interfaces;
using System.Runtime.CompilerServices;

namespace SimpleTimeMan.Classes
{
    class Logger : iLogger
    {
        const string LogTag = "LogTag";
        const string Separator = "\n<Newline>\n";
        iDataWorker DataWorker;
        public Logger(iDataWorker dataworker)
        {
            DataWorker = dataworker;
            DataWorker.SaveData(LogTag, "");
        }
        public void WriteLog(string data,
            [CallerLineNumber] int num = -1,
            [CallerFilePath] string path = "",
            [CallerMemberName] string name = "") => DataWorker.SaveData(LogTag, DataWorker.LoadData(LogTag) + $"{data}\n{path}:{num} ({name})" + Separator);
        public string[] LoadAllLog() => DataWorker.LoadData(LogTag).Split(Separator);
    }
}