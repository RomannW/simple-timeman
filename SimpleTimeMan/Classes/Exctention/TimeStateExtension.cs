﻿using SimpleTimeManClassLibrary.Classes;
using Android.Support.V7.App;
using SimpleTimeMan.Activity;
using SimpleTimeMan.Model;
using Android.Content;
using Android.Widget;
using Android.Views;

namespace SimpleTimeMan.Classes.Exctention
{
    public static class TimeStateExtension
    {
        public static View GetView(this TimeState state, AppCompatActivity context, ViewGroup parentlayout)
        {
            View Result = LayoutInflater.From(context).Inflate(Resource.Layout.view_timeevent, parentlayout, false);
            Result.FindViewById<TextView>(Resource.Id.TimeTextView).Text = state.StartTime.ToString("H:mm:ss");
            Result.FindViewById<TextView>(Resource.Id.StatusTextView).Text = state.Status.ToString();
            Result.FindViewById<TextView>(Resource.Id.StatusTextView).SetTextColor(Helper.GetColorByState(state.Status));
            Result.Click += (s, e) => new Fragment_EditEvent(state).Show(context);
            return Result;
        }
    }
}