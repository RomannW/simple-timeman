﻿using Android.Graphics.Drawables;
using Android.Support.V7.App;
using SimpleTimeMan.Activity;
using Android.Graphics;
using Android.Views;
using Android.App;
using System;

namespace SimpleTimeMan.Classes
{
    class CustomFragment : DialogFragment
    {
        public CustomFragment() : base() { }
        public CustomFragment(IntPtr Reference, Android.Runtime.JniHandleOwnership Transfer) : base(Reference, Transfer) { }
        const string Dialog_str = "dialog";
        bool IsStarted = false;
        static CustomFragment StartedInstance;
        protected AppCompatActivity CustomContext;
        public bool Show(AppCompatActivity context)
        {
            if (context != null && (StartedInstance == null || !StartedInstance.IsStarted || StartedInstance.GetType() != GetType()))
            {
                try { StartedInstance?.Dismiss(); } catch (Exception) { }
                CustomContext = context;
                StartedInstance = this;
                IsStarted = true;
                base.Show(context.FragmentManager, Dialog_str);
                return true;
            }
            return false;
        }
        public override void OnDestroyView()
        {
            if (CustomContext is CustomActivity CustomActivity)
                CustomActivity.Refrash();
            base.OnDestroyView();
        }
        public override void OnDestroy()
        {
            IsStarted = false;
            if (StartedInstance == this)
                StartedInstance = null;
            base.OnDestroy();
        }
        public override void OnResume()
        {
            Dialog.Window.SetLayout(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
            Dialog.Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));
            SetStyle(DialogFragmentStyle.NoFrame, Android.Resource.Style.Theme);
            base.OnResume();
        }
    }
}