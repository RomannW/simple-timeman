﻿using SimpleTimeManClassLibrary.Classes;
using SimpleTimeMan.Model;
using Android.Graphics;
using Android.Content;
using Android.Util;
using System;

namespace SimpleTimeMan.Classes
{
    public class CustomTimeLine : BaseCustomTimeLine
    {
        public CustomTimeLine(Context c, IAttributeSet a) : base(c, a) { }
        public CustomTimeLine(Context c, IAttributeSet a, int ds) : base(c, a, ds) { }
        float Progress = 0.0F;
        const float ScaleHeight = 15.0F;
        protected override void OnDraw(Canvas canvas)
        {
            Progress = 0.0F;
            canvas.DrawColor(Color.Transparent);
            canvas.DrawRect(
                0, Math.Max(0, Height - Width / ScaleHeight),
                Width, Height,
                new Paint() { AntiAlias = true, Color = Color.Argb(0x22, 0xFF, 0xFF, 0xFF) });
            foreach (TimeState State in TimeStates)
            {
                canvas.DrawRect(
                    Progress * Width,
                    Math.Max(0, Height - Width / ScaleHeight),
                    (Progress + PpmFromState(State, TimeStates)) * Width,
                    Height,
                    new Paint() { AntiAlias = true, Color = Helper.GetColorByState(State.Status) });
                Progress += PpmFromState(State, TimeStates);
            }
            base.OnDraw(canvas);
        }
    }
}