﻿using SimpleTimeManClassLibrary.Classes;
using Android.Content;
using System.Timers;
using Android.Views;
using Android.Util;
using System.Linq;
using System;
using SimpleTimeMan.Model;

namespace SimpleTimeMan.Classes
{
    public class BaseCustomTimeLine : View
    {
        public BaseCustomTimeLine(Context c, IAttributeSet a) : base(c, a) { }
        public BaseCustomTimeLine(Context c, IAttributeSet a, int ds) : base(c, a, ds) { }
        Timer InvalidateTimer;
        TimeState[] ts = new TimeState[] { };
        public TimeState[] TimeStates
        {
            get => ts;
            set
            {
                ts = value;
                Invalidate();
                if (InvalidateTimer == null)
                {
                    InvalidateTimer = new Timer(1000 * 5.0);
                    InvalidateTimer.Elapsed += (s, e) => Invalidate();
                    InvalidateTimer.Start();
                }
            }
        }
        protected float PpmFromState(TimeState State, TimeState[] states)
        {
            TimeSpan AllDuration = State.StartTime.Date == DateTime.Now.Date ? MainModel.AllDuration : states.Last().EndTime - states.First().StartTime;
            return (int)((float)(State.Duration.TotalSeconds / (float)AllDuration.TotalSeconds) * 1000) / 1000.0F;
        }
    }
}