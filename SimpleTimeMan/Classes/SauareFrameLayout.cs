﻿using Android.Content;
using Android.Widget;
using Android.Util;
using System;

namespace SimpleTimeMan.Classes
{
    public class SauareFrameLayout : FrameLayout
    {
        public SauareFrameLayout(Context c, IAttributeSet a) : base(c, a) { }
        public SauareFrameLayout(Context c, IAttributeSet a, int ds) : base(c, a, ds) { }
        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) => base.OnMeasure(widthMeasureSpec, Math.Min(widthMeasureSpec, heightMeasureSpec));
    }
}