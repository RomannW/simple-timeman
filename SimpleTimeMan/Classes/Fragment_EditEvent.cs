﻿using SimpleTimeManClassLibrary.Classes;
using SimpleTimeMan.Model;
using Android.Widget;
using Android.Views;
using Android.App;
using Android.OS;
using System;

namespace SimpleTimeMan.Classes
{
    class Fragment_EditEvent : CustomFragment
    {
        public const string TimeEventTag = "TimeEventTag";
        TimeState State;
        TextView TimeLabel;
        DateTime net;
        DateTime NewEventTime
        {
            get => net;
            set
            {
                net = value;
                TimeLabel.Text = net.ToString("HH:mm:ss");
            }
        }
        public Fragment_EditEvent(TimeState state)
        {
            State = state;
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            View RootView = inflater.Inflate(Resource.Layout.fragment_editevent, container, false);
            TimeLabel = RootView.FindViewById<TextView>(Resource.Id.TimeLabel);
            RootView.FindViewById<Button>(Resource.Id.ApplySetting).Click += ApplySetting_Click;
            RootView.FindViewById<Button>(Resource.Id.RemoveState).Click += RemoveEvent_Click;
            RootView.FindViewById<SeekBar>(Resource.Id.TimeSetting).ProgressChanged += TimeSetting_ProgressChanged;
            RootView.FindViewById<TextView>(Resource.Id.StatusLabel).Text = State.Status.ToString();
            RootView.FindViewById<TextView>(Resource.Id.StatusLabel).SetTextColor(Helper.GetColorByState(State.Status));
            NewEventTime = State.StartTime;
            return RootView;
        }
        void ApplySetting_Click(object s, EventArgs e)
        {
            MainModel.EditStateStartTime(State, NewEventTime);
            Dismiss();
        }
        void RemoveEvent_Click(object sender, EventArgs e)
        {
            MainModel.RemoveTimeState(State);
            Dismiss();
        }
        void TimeSetting_ProgressChanged(object s, SeekBar.ProgressChangedEventArgs e) => NewEventTime = State.StartTime.AddSeconds(GetModifyValue(e.Progress) * e.Progress);
        double GetModifyValue(double x) => x == 0 ? 0 : (x * x / 85000 + 1);
    }
}