﻿using SimpleTimeManClassLibrary.Interfaces;
using SimpleTimeMan.Activity;

namespace SimpleTimeMan.Classes
{
    class DataWorker : iDataWorker
    {
        const string SettingsFile = "SimpleTimeManSettings";
        static object Locker = new object();
        Android.Content.ISharedPreferences Preferences => CustomActivity.Context.GetSharedPreferences(SettingsFile, Android.Content.FileCreationMode.Private);
        public string LoadData(string key, string def = "")
        {
            lock (Locker)
            {
                return Preferences.GetString(key, def);
            }
        }
        public void SaveData(string key, string value)
        {
            lock (Locker)
            {
                Android.Content.ISharedPreferencesEditor Editor = Preferences.Edit();
                Editor.PutString(key, value);
                bool Result = Editor.Commit();
            }
        }
    }
}