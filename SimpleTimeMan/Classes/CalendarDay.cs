﻿using SimpleTimeManClassLibrary.Classes;
using System.Collections.Generic;
using Android.Support.V7.App;
using SimpleTimeMan.Activity;
using SimpleTimeMan.Model;
using Android.Graphics;
using Android.Content;
using Android.Widget;
using Android.Views;
using System.Linq;
using Android;
using System;

namespace SimpleTimeMan.Classes
{
    class CalendarDay
    {
        static CalendarYearInfo Info;
        CalendarYearInfo GetYearInfo(DateTime date)
        {
            if (Info?.Year != date.Year)
                Info = MainModel.LoadHolydays(date);
            return Info;
        }
        public CalendarDay(DateTime date, int w, int d, bool cm)
        {
            Date = date;
            Week = w;
            Day = d;
            IsCurrentMonth = cm;
            Type = GetYearInfo(date).Months.FirstOrDefault(m => m.Month == date.Month)?.Days.FirstOrDefault(d => d.Day == date.Day)?.DayType ?? CalendarDayInfo.DayTypes.Work;
            DayInfo = MainModel.GetDayInfo(date);
        }
        public static List<CalendarDay> GetMonth(DateTime date)
        {
            List<CalendarDay> Result = new List<CalendarDay>();
            DateTime CurrentDate = new DateTime(date.Year, date.Month, 1);
            if (CurrentDate.DayOfWeek == DayOfWeek.Monday)
                CurrentDate = CurrentDate.AddDays(-7);
            else
                CurrentDate = CurrentDate.AddDays(-(((int)CurrentDate.DayOfWeek + 6) % 7));
            for (int w = 0; w < 6; w++)
            {
                for (int d = 0; d < 7; d++)
                {
                    Result.Add(new CalendarDay(CurrentDate, w, d, CurrentDate.Month == date.Month));
                    CurrentDate = CurrentDate.AddDays(1);
                }
            }
            return Result;
        }
        public int Day { get; }
        public int Week { get; }
        public DateTime Date { get; }
        public bool IsCurrentMonth { get; }
        CalendarDayInfo.DayTypes Type { get; }
        DayInfo DayInfo { get; }
        View ThisView = null;
        public View GetView(AppCompatActivity context, LinearLayout parentlayout)
        {
            ThisView = LayoutInflater.From(context).Inflate(Resource.Layout.view_day, parentlayout, false);
            ThisView.FindViewById<TextView>(Resource.Id.DayNum).Text = Date.ToString("dd");
            if (IsCurrentMonth)
            {
                if (Date.Date == DateTime.Now.Date)
                    ThisView.FindViewById(Resource.Id.TodayIcon).Visibility = ViewStates.Visible;
                switch (Type)
                {
                    case CalendarDayInfo.DayTypes.Short:
                        ThisView.FindViewById<TextView>(Resource.Id.DayNum).Text += "*";
                        break;
                    case CalendarDayInfo.DayTypes.DayOff:
                        ThisView.FindViewById<TextView>(Resource.Id.DayNum).SetTextColor(Color.Rgb(0xFF, 0x88, 0x88));
                        break;
                    case CalendarDayInfo.DayTypes.Work:
                    default:
                        break;
                }
                ThisView.FindViewById<CustomTimeLine>(Resource.Id.Diagram).TimeStates = DayInfo.TimeStates;
            }
            else
            {
                ThisView.FindViewById<TextView>(Resource.Id.DayNum).Alpha = 0.35F;
                ThisView.FindViewById<CustomTimeLine>(Resource.Id.Diagram).Visibility = ViewStates.Invisible;
            }
            ThisView.Click += (s, e) =>
            {
                //Intent Intent = new Intent(context, typeof(EditEvent));
                //Intent.PutExtra(TimeEventTag, GetString());
                //context.StartActivity(Intent);
            };
            ((LinearLayout.LayoutParams)ThisView.LayoutParameters).Weight = 1;
            ((LinearLayout.LayoutParams)ThisView.LayoutParameters).SetMargins(2, 2, 2, 2);
            ThisView.Click += (s, e) =>
            {
                Intent Intent = new Intent(context, typeof(EventList));
                Intent.PutExtra(EventList.DateTag, Date.ToString());
                context.StartActivity(Intent);
            };
            return ThisView;
        }
        public static string GetStatisticsMonth(List<CalendarDay> days, DateTime date)
        {
            List<CalendarDay> Days = days.Where(d => d.Date.Month == date.Month).ToList();
            int DaysInMonth = Days.Count();
            int WorkDaysInMonth = Days.Where(d => d.Type == CalendarDayInfo.DayTypes.Work || d.Type == CalendarDayInfo.DayTypes.Short).Count();
            int OfficeDaysInMonth = WorkDaysInMonth % 2 == 0 ? WorkDaysInMonth / 2 : WorkDaysInMonth / 2 + 1;
            int WorkedDaysInMonth = Days.Where(d => d.DayInfo.TimeStates.Where(t => t.Status == States.Work).Count() > 0).Count();
            int Percent = 100;
            if (OfficeDaysInMonth > 0 && WorkedDaysInMonth < OfficeDaysInMonth)
                Percent = (int)((float)WorkedDaysInMonth / (float)OfficeDaysInMonth * 100);
            return $"Days: {DaysInMonth}\nWorkdays: {WorkDaysInMonth}\nWorked: {WorkedDaysInMonth} / {OfficeDaysInMonth}({Percent} %)";
        }
    }
}
//static DateTime[] ExtraHolydays = new DateTime[]
//{
//    new DateTime(2021, 1, 1),
//    new DateTime(2021, 1, 4),
//    new DateTime(2021, 1, 5),
//    new DateTime(2021, 1, 6),
//    new DateTime(2021, 1, 7),
//    new DateTime(2021, 1, 8),

//    new DateTime(2021, 2, 22),
//    new DateTime(2021, 2, 23),

//    new DateTime(2021, 3, 8),

//    new DateTime(2021, 5, 3),
//    new DateTime(2021, 5, 10),

//    new DateTime(2021, 6, 14),

//    new DateTime(2021, 11, 4),
//    new DateTime(2021, 11, 5),

//    new DateTime(2021, 12, 31   ),
//};
//static DateTime[] ExtraWorkDays = new DateTime[]
//{
//    new DateTime(2021, 2, 20),
//};