﻿using SimpleTimeManClassLibrary.Classes;
using SimpleTimeMan.Model;
using Android.Graphics;
using Android.Content;
using Android.Util;
using System;

namespace SimpleTimeMan.Classes
{
    public class CustomCircleTimeLine : BaseCustomTimeLine
    {
        public CustomCircleTimeLine(Context c, IAttributeSet a) : base(c, a) { }
        public CustomCircleTimeLine(Context c, IAttributeSet a, int ds) : base(c, a, ds) { }
        int Size = 0;
        int StartX = 0;
        int StartY = 0;
        int StartAngle = 0;
        const int RotateConst = -90;
        const int StartAngleConst = 135;
        float LineWidth => (int)(3.0F / 9.0F * (float)Size / 2.0F);
        int MaxSweepAngleConst => 360 - (StartAngleConst + RotateConst) * 2;
        protected override void OnDraw(Canvas canvas)
        {
            canvas.DrawColor(Color.Transparent);
            #region Start; Suze; Center;
            StartX = (Width > Height) ? (Width - Height) / 2 : 0;
            StartY = (Height > Width) ? (Height - Width) / 2 : 0;
            Size = Math.Min(Height, Width);
            #endregion
            StartAngle = StartAngleConst;
            DrawBGArc(canvas);
            foreach (TimeState State in TimeStates)
                DrawArc(canvas, (int)(PpmFromState(State, TimeStates) * MaxSweepAngleConst), Helper.GetColorByState(State.Status));
            base.OnDraw(canvas);
        }
        void DrawBGArc(Canvas canvas)
        {
            Path path = new Path();
            path.ArcTo(StartX, StartY, StartX + Size, StartY + Size, StartAngle, MaxSweepAngleConst, false);
            path.ArcTo(StartX + LineWidth, StartY + LineWidth, StartX + Size - LineWidth, StartY + Size - LineWidth, 45, -MaxSweepAngleConst, false);
            canvas.DrawPath(path, new Paint() { Color = Color.Argb(0x22, 0xFF, 0xFF, 0xFF), AntiAlias = true, StrokeWidth = 50 });
        }
        void DrawArc(Canvas canvas, int sweepAngle, Color color)
        {
            Path path = new Path();
            path.ArcTo(StartX, StartY, StartX + Size, StartY + Size, StartAngle, sweepAngle, false);
            path.ArcTo(StartX + LineWidth, StartY + LineWidth, StartX + Size - LineWidth, StartY + Size - LineWidth, StartAngle + sweepAngle, -sweepAngle, false);
            canvas.DrawPath(path, new Paint() { Color = color, AntiAlias = true, StrokeWidth = 50 });
            StartAngle += sweepAngle;
        }
    }
}